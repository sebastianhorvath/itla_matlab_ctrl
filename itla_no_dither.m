function [] = itla_no_dither(sobj)
%ITLA_NO_DITHER Enable the "no-dither mode".

m = ['01'; '90'; '00'; '01'];
itla_send_msg(sobj, m);
itla_rec_msg(sobj);

end

