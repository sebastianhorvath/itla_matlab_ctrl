function [] = itla_ret_std_op(sobj)
%ITLA_RET_STD_OP Return the laser to standard operating mode. Call after
%stopping sweep.

m = ['01'; '90'; '00'; '00'];
itla_send_msg(sobj, m);
itla_rec_msg(sobj);

end

