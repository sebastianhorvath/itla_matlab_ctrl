function [ msg ] = itla_rec_msg(sobj)
%ITLA_REC_MSG Receive 4 bytes from the ITLA and convert to hex.

buf = [];
while true
   c = fread(sobj,1);
   if isempty(c)
       break
   end  
   buf = [buf c];
   if length(buf) == 4
       break
   end
end
bip4 = itla_calc_bip4(buf);
if bip4 ~= bitsra(uint8(buf(1)), 4)
    warning('Checksum failed for received message');
end

msg = [];
for i=1:4
    msg = [msg; dec2hex(buf(i), 2)];
end




