function[] = itla_start_sweep(sobj)
%ITLA_START_SWEEP Start ITLA clean sweep.

m = ['01'; 'E5'; '00'; '01'];
itla_send_msg(sobj, m);
itla_rec_msg(sobj);

end

