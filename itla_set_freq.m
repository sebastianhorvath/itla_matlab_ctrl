function [] = itla_set_freq(sobj, freq)
%ITLA_SET_FREQ Set the central frequency of the ITLA laser in the range
%191.5-196.25 THz. Note: ITLA must be off for this to take effect.


if (freq < 191.5) || (freq > 196.25)
    error('ITLA frequency must be in the range 191.5-196.25 THz')
end

f1 = floor(freq);               % The THz component is written to FCF1
f2 = floor((freq-f1)*10000);    % The 0.1 GHz component is written to FCF2

f1 = dec2hex(f1, 4); 
f2 = dec2hex(f2, 4);

m1 = ['01'; '35'; f1(1:2); f1(3:4)];
m2 = ['01'; '36'; f2(1:2); f2(3:4)];

itla_send_msg(sobj, m1);
itla_rec_msg(sobj);

itla_send_msg(sobj, m2);
itla_rec_msg(sobj);

end

