function [ret] = itla_read_nop(sobj)
%ITLA_READ_NOP Read and return the value of the NOP register.

m = ['00'; '00'; '00'; '00'];
itla_send_msg(sobj, m);
ret = itla_rec_msg(sobj);

end

