function [] = itla_set_ch(sobj, ch)
%ITLA_SET_CH Set the channel for the ITLA.

c = dec2hex(ch, 2);
m = ['01'; '30'; '00'; c];
itla_send_msg(sobj, m);
itla_rec_msg(sobj);

end

