function [ bip4 ] = calc_bip4(data)
%CALC_BIP4 Checksum for data packet consiting of 4 bytes.

bip8 = uint8(bitxor(bitxor(bitxor(bitand(data(1), uint8(15)), data(2)), data(3)), data(4)));
bip4 = uint8(bitxor(bitsra(bitand(bip8, uint8(240)), 4), bitand(bip8, uint8(15))));

end

