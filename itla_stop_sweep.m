function [] = itla_stop_sweep(sobj)
%ITLA_STOP_SWEEP Stop ITLA clean sweep.

m = ['01'; 'E5'; '00'; '00'];
itla_send_msg(sobj, m);
itla_rec_msg(sobj);
end

