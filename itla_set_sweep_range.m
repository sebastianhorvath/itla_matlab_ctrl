function [] = itla_set_sweep_range(sobj, range)
%ITLA_SET_SWEEP_RANGE Set the clean sweep range in GHz.

if range > 150
    error('Maximum sweep range of 150 GHz exceeded')
end

r = dec2hex(round(range), 2);
m = ['01'; 'E4'; '00'; r];

itla_send_msg(sobj, m);
itla_rec_msg(sobj);
end

