function [] = itla_on(sobj)
%ITLA_ON Enable the ITLA laser output.

m = ['01'; '32'; '00'; '08'];
itla_send_msg(sobj, m);
itla_rec_msg(sobj);

% Wait until ITLA status byte returns without pending flag.
ret = itla_read_nop(sobj);

while ret(3) == 1
    ret = itla_read_nop(sobj);
end
end

