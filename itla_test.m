% Open serial port
if exist('serport','var')
    fclose(serport); %#ok<NODEF>
    fopen(serport);
else
% Global serport; if script is interupted, we can still close connection to ITLA in command window.
% Persistent serport - this means the value of serport is retained between calls to this function. 
    serport = serial('COM2');
    set(serport,'BaudRate',9600);
    set(serport,'DataBits',8);
    set(serport,'StopBits',1);
    set(serport,'Parity','none');
    set(serport,'Timeout',3);
    fopen(serport);
end

freq = linspace(191.5,191.6,10);
itla_set_freq(serport, freq(1));
itla_on(serport);
itla_no_dither(serport);
pause(2);
for i=2:10
    i
    pause(1);
    itla_ret_std_op(serport);
    itla_off(serport);
    itla_set_freq(serport, freq(i));
    itla_on(serport);
    itla_no_dither(serport);
end

itla_ret_std_op(serport);
itla_off(serport);
fclose(serport);





