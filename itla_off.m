function [] = itla_off(sobj)
%ITLA_OFF Disable the ITLA laser.

m = ['01'; '32'; '00'; '00'];
itla_send_msg(sobj, m);
itla_rec_msg(sobj);

end

