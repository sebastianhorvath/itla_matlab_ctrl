function [] = itla_send_msg(sobj, msg)
%ITLA_SEND_MSG Send 4 hex bytes to ITLA laser.

if iscellstr(msg) ~= 1
    msg = cellstr(msg);
end

buf = [];
for i = 1:4
    buf = [buf uint8(hex2dec(msg(i)))];
end

bip4 = itla_calc_bip4(buf);
buf(1) = uint8(bitxor(buf(1), bitshift(bip4, 4)));

fwrite(sobj, buf);


